from random import randint

class Square:
    def __init__(self, number):
        self.number = number
        if number % 2 == 0:
            self.colour = 'White'
        else:
            self.colour = 'Grey'

class Snake:
    def __init__(self,nextSquare):
        self.name = 'Snake'
        self.colour = 'Red'
        self.nextSquare = nextSquare

class Ladder:
    def __init__(self,nextSquare):
        self.name = 'Ladder'
        self.colour = 'Green'
        self.nextSquare = nextSquare

class Board:
    matrix = [[]]

    def __init__(self, size):
        self.size = size
        self.matrix = [[0 for x in range(size)] for y in range(size)]
        counter = 0

        for vert in range(0, size):
            if vert % 2 == 0:
                for horiz in range(0, size):
                    counter += 1
                    self.matrix[vert][horiz] = Square(counter)
            else:
                for horiz in reversed(range(0, size)):
                    counter += 1
                    self.matrix[vert][horiz] = Square(counter)

    def getCoords(self,square):
        x = 0
        y = 0
        for vert in range(0, self.size):
            for horiz in range(0, self.size):
                if self.matrix[vert][horiz].number == square:
                    x = horiz
                    y = vert
        return x, y

    def addLadder(self,square,result):
        x = self.getCoords(square)[0]
        y = self.getCoords(square)[1]
        self.matrix[y][x].__class__ = Ladder
        self.matrix[y][x].__init__(result)

    def addSnake(self,square,result):
        x = self.getCoords(square)[0]
        y = self.getCoords(square)[1]
        self.matrix[y][x].__class__ = Snake
        self.matrix[y][x].__init__(result)

    def addSquares(self):
        squareCount = int(self.size / 2)
        print(str(squareCount) + ' snakes or ladders!')

        numLadders = randint(0,squareCount)
        numSnakes = squareCount - numLadders

        print(str(numLadders) + " Ladders")
        print(str(numSnakes) + " Snakes")

        if numLadders != 0:
            for square in range(1, numLadders+1):
                position = randint(1, (self.size**2) - 2)
                result = randint(position, (self.size**2) - 1)
                self.addLadder(position, result)
                print("ladder added to square " + str(position) + " up to " + str(result))

        if numSnakes != 0:
            for square in range(1, numSnakes+1):
                position = randint(1, (self.size**2) - 2)
                result = randint(1, position)
                self.addSnake(position, result)
                print("snake added to square " + str(position) + " down to " + str(result))