#import exceptions
import player
import board
import gui
from random import randint


##########################
#                        #
#       Main Script      #
#                        #
##########################

splash = gui.Splash()


configGui = gui.gameConfig()
numPlayers = configGui.number
boardSize = configGui.board

Board = board.Board(boardSize)

# Board.addSquares()

#create array of players
players = []
for i in range(0, numPlayers):
    nameGui = gui.nameEntry(str(i+1))
    playerName = nameGui.name
    players.append(player.Player(playerName,i+1))

Board.addSquares()

game = gui.gameBoardInit(Board)

gameOn = True
while gameOn == True:
    for i in range(0, numPlayers):
        #input('It is your go ' + players[i].name + '.  Hit any key to roll.')
        diceRoll = randint(1,6)
        print('You rolled a ' + str(diceRoll))

        if players[i].currentSquare + diceRoll >= boardSize**2:
            print(players[i].name + ' is the winner!  Good game!!')
            winGui = gui.Win(players[i].name)
            gameOn = False
            break

        if gameOn:
            print('You move from ' + str(players[i].currentSquare) + ' to ' + str(players[i].currentSquare + diceRoll))



            players[i].currentSquare += diceRoll

            # boardGui = gui.gameBoard(Board, players, players[i].currentSquare, diceRoll)

            x = Board.getCoords(players[i].currentSquare)[0]
            y = Board.getCoords(players[i].currentSquare)[1]

            if Board.matrix[y][x].__class__ == board.Snake:
                print('You landed on a Snake!  You slide down to ' + str(Board.matrix[y][x].nextSquare))
                message = gui.SquareMessage('Snake', str(Board.matrix[y][x].nextSquare))
                players[i].currentSquare = Board.matrix[y][x].nextSquare
            elif Board.matrix[y][x].__class__ == board.Ladder:
                print('You landed on a Ladder!  You climb up to ' + str(Board.matrix[y][x].nextSquare))
                message = gui.SquareMessage('Ladder', str(Board.matrix[y][x].nextSquare))
                players[i].currentSquare = Board.matrix[y][x].nextSquare

            boardGui = gui.gameBoard(Board, players, players[i].name, players[i].currentSquare, diceRoll)
