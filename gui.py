from tkinter import *


class Splash:
    def __init__(self):
        root = Tk(className="Snakes and Ladders")

        #http://stackoverflow.com/questions/14910858/how-to-specify-where-a-tkinter-window-opens
        w = 640  # width for the Tk root
        h = 480  # height for the Tk root

        # get screen width and height
        ws = root.winfo_screenwidth()  # width of the screen
        hs = root.winfo_screenheight()  # height of the screen

        # calculate x and y coordinates for the Tk root window
        x = (ws / 2) - (w / 2)
        y = (hs / 2) - (h / 2)

        # set the dimensions of the screen
        # and where it is placed
        root.geometry('%dx%d+%d+%d' % (w, h, x, y))


        def act():
            root.destroy()

        # http://www.snakes-and-ladders.co.uk/wp-content/uploads/2015/10/Snakes-and-Ladders.gif
        photo = PhotoImage(file="Snakes-and-Ladders.gif")
        logo = Label(image=photo)
        logo.image = photo
        logo.pack()


        label = Label(root, text="Snakes and Ladders Game")
        button = Button(root, text="Play Game", command=act)

        label.pack()
        logo.pack()
        button.pack()

        root.mainloop()

class nameEntry:
    def __init__(self, playerNumber):
        self.name = ''
        root = Tk(className="Enter name of player " + playerNumber)

        # http://stackoverflow.com/questions/14910858/how-to-specify-where-a-tkinter-window-opens
        w = 250  # width for the Tk root
        h = 100  # height for the Tk root

        # get screen width and height
        ws = root.winfo_screenwidth()  # width of the screen
        hs = root.winfo_screenheight()  # height of the screen

        # calculate x and y coordinates for the Tk root window
        x = (ws / 2) - (w / 2)
        y = (hs / 2) - (h / 2)

        # set the dimensions of the screen
        # and where it is placed
        root.geometry('%dx%d+%d+%d' % (w, h, x, y))

        Label(root, text="Enter Name of Player " + playerNumber, justify=LEFT, padx=20).pack()
        self.nameWidget = StringVar()
        entry = Entry(root, textvariable=self.nameWidget)
        entry.pack()

        def act():
            #print('%s' % self.nameWidget.get())
            self.name = self.nameWidget.get()
            root.destroy()

        button = Button(root, text="Enter", command=act)
        button.pack()
        root.mainloop()

class gameConfig:
    def __init__(self):
        self.number = 1
        self.board = 8
        root = Tk(className="Set Game Settings")

        # http://stackoverflow.com/questions/14910858/how-to-specify-where-a-tkinter-window-opens
        w = 250  # width for the Tk root
        h = 250  # height for the Tk root

        # get screen width and height
        ws = root.winfo_screenwidth()  # width of the screen
        hs = root.winfo_screenheight()  # height of the screen

        # calculate x and y coordinates for the Tk root window
        x = (ws / 2) - (w / 2)
        y = (hs / 2) - (h / 2)

        # set the dimensions of the screen
        # and where it is placed
        root.geometry('%dx%d+%d+%d' % (w, h, x, y))


        self.numberWidget = IntVar()
        self.numberWidget.set(1)
        self.scaleWidget = Scale(root, from_=4, to=10, orient=HORIZONTAL)
        self.scaleWidget.set(8)

        Label(root, text="Set Board Size:", justify=LEFT, padx=20).pack()
        self.scaleWidget.pack()

        Label(root, text="Number of Players:", justify=LEFT, padx=20).pack()
        Radiobutton(root, text="One",   variable=self.numberWidget, value=1).pack(anchor=W)
        Radiobutton(root, text="Two",   variable=self.numberWidget, value=2).pack(anchor=W)
        Radiobutton(root, text="Three", variable=self.numberWidget, value=3).pack(anchor=W)
        Radiobutton(root, text="Four",  variable=self.numberWidget, value=4).pack(anchor=W)

        def act():
            self.number = self.numberWidget.get()
            self.board = self.scaleWidget.get()
            root.destroy()

        button = Button(root, text="Enter", command=act)
        button.pack()

        mainloop()

class gameBoardInit:
    def __init__(self, board):
        root = Tk(className="Snakes and Ladders")
        # http://stackoverflow.com/questions/14910858/how-to-specify-where-a-tkinter-window-opens
        w = 800  # width for the Tk root
        h = 650  # height for the Tk root

        # get screen width and height
        ws = root.winfo_screenwidth()  # width of the screen
        hs = root.winfo_screenheight()  # height of the screen

        # calculate x and y coordinates for the Tk root window
        x = (ws / 2) - (w / 2)
        y = (hs / 2) - (h / 2)

        # set the dimensions of the screen
        # and where it is placed
        root.geometry('%dx%d+%d+%d' % (w, h, x, y))

        #layout assistance from http://stackoverflow.com/questions/2763266/grid-within-a-frame

        f = Frame(root, bg="orange", width=800, height=650)
        f.pack(side=LEFT, expand=1)

        f3 = Frame(f, bg="red", width=800)
        f3.pack(side=LEFT, expand=1, pady=50, padx=50)

        f2 = Frame(root, bg="black", height=100, width=100)
        f2.pack(side=LEFT, fill=Y)

        for horiz in reversed(range(0,board.size)):
            for vert in reversed(range(0,board.size)):
                canvas = Canvas(f3, width=50, height=50)
                canvas.grid(row=board.size-vert, column=board.size-horiz)
                colour = board.matrix[vert][horiz].colour
                canvas.create_rectangle(0, 0, 50, 50, fill=colour)
                canvas.create_text(40,40, text=board.matrix[vert][horiz].number,fill="black",font="10")

        def Begin():
            root.destroy()

        b = Button(f2, text="Begin", command=Begin)
        b.pack()

        root.mainloop()

class gameBoard:
    def __init__(self, board, players, currentPl, currentSq, roll):
        root = Tk(className="Snakes and Ladders")
        # http://stackoverflow.com/questions/14910858/how-to-specify-where-a-tkinter-window-opens
        w = 800  # width for the Tk root
        h = 650  # height for the Tk root

        # get screen width and height
        ws = root.winfo_screenwidth()  # width of the screen
        hs = root.winfo_screenheight()  # height of the screen

        # calculate x and y coordinates for the Tk root window
        x = (ws / 2) - (w / 2)
        y = (hs / 2) - (h / 2)

        # set the dimensions of the screen
        # and where it is placed
        root.geometry('%dx%d+%d+%d' % (w, h, x, y))

        #layout assistance from http://stackoverflow.com/questions/2763266/grid-within-a-frame

        f = Frame(root, bg="orange", width=800, height=650)
        f.pack(side=LEFT, expand=1)

        Label(f, text=currentPl + " moves to " + str(currentSq)).pack()

        f3 = Frame(f, bg="red", width=800, height=650)
        f3.pack(side=LEFT, expand=1, pady=50, padx=50)

        f2 = Frame(root, bg="black", height=100, width=100)
        f2.pack(side=LEFT, fill=Y)

        for horiz in reversed(range(0,board.size)):
            for vert in reversed(range(0,board.size)):
                canvas = Canvas(f3, width=50, height=50)
                canvas.grid(row=board.size-vert, column=board.size-horiz)
                colour = board.matrix[vert][horiz].colour
                canvas.create_rectangle(0, 0, 50, 50, fill=colour)
                canvas.create_text(40,40, text=board.matrix[vert][horiz].number,fill="black",font="10")

                for i in range(0, len(players)):
                    if players[i].currentSquare == board.matrix[vert][horiz].number:
                        canvas.create_oval(10, 10, 20, 20, width=1, fill=players[i].colour)

        def Begin():
            root.destroy()

        b = Button(f2, text="Roll", command=Begin)
        diceRoll = Label(f2, text=roll)
        b.pack()
        diceRoll.pack()

        root.mainloop()

class Win:
    def __init__(self, player):
        root = Tk(className="Winner")

        # http://stackoverflow.com/questions/14910858/how-to-specify-where-a-tkinter-window-opens
        w = 250  # width for the Tk root
        h = 100  # height for the Tk root

        # get screen width and height
        ws = root.winfo_screenwidth()  # width of the screen
        hs = root.winfo_screenheight()  # height of the screen

        # calculate x and y coordinates for the Tk root window
        x = (ws / 2) - (w / 2)
        y = (hs / 2) - (h / 2)

        # set the dimensions of the screen
        # and where it is placed
        root.geometry('%dx%d+%d+%d' % (w, h, x, y))

        def act():
            root.destroy()

        label = Label(root, text=player + " is the winner")
        button = Button(root, text="Exit", command=act)

        label.pack()
        button.pack()

        root.mainloop()

class SquareMessage:
    def __init__(self, type, nextSq):
        root = Tk(className=type)

        # http://stackoverflow.com/questions/14910858/how-to-specify-where-a-tkinter-window-opens
        w = 400  # width for the Tk root
        h = 100  # height for the Tk root

        # get screen width and height
        ws = root.winfo_screenwidth()  # width of the screen
        hs = root.winfo_screenheight()  # height of the screen

        # calculate x and y coordinates for the Tk root window
        x = (ws / 2) - (w / 2)
        y = (hs / 2) - (h / 2)

        # set the dimensions of the screen
        # and where it is placed
        root.geometry('%dx%d+%d+%d' % (w, h, x, y))

        def act():
            root.destroy()

        label = Label(root, text="You landed on a " + type + "! you move to " + nextSq)
        button = Button(root, text="Exit", command=act)

        label.pack()
        button.pack()

        root.mainloop()