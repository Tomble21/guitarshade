# define user-defined exceptions
class Error(Exception):
   """Base class for other exceptions"""
   pass

class InvalidPlayers(Error):
   """Raised when the input value not between 1 and 4"""
   pass

# class ValueTooLargeError(Error):
#    """Raised when the input value is too large"""
#    pass